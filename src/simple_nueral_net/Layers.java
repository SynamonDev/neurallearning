package simple_nueral_net;

import java.util.ArrayList;

public class Layers {

	//int numberOfNeurons;
	ArrayList<NeuronA> NeuronArray= new ArrayList<NeuronA>();
	ArrayList<Double> outputArrayList = new ArrayList <Double>();
	ArrayList<Double> inputArrayList = new ArrayList <Double>();
	
	public Layers(int numberOfNuerons, ArrayList<Double> inputArray){
		for(int i=0; i< numberOfNuerons;i++) {
			NeuronA n = new NeuronA(inputArray);
			NeuronArray.add(n);
		}
		inputArrayList = inputArray;
	}
	
	public ArrayList<Double> setOutput(){
		ArrayList<Double> newoutputArrayList = new ArrayList <Double>();
		for(NeuronA n:NeuronArray){
			newoutputArrayList.add(n.firingFunction());
		}
		this.outputArrayList = newoutputArrayList;
		return outputArrayList;
	}
	
	public ArrayList<Double> getOutput(){
		return this.outputArrayList;
	}
	
	public void adaptWeights(int tip){
//		for(NeuronA n : NeuronArray){
//			ArrayList<Double> newWeights = ArrayList<Double>();
//			for( double w: n.getWeight()){
//				newWeights.add( n + 1 (tip - ))
//			}
//			
//			n.setWeights(newWegiths);
//		}
		
		NeuronA n = NeuronArray.get(0);
		ArrayList<Double> newWeights = new ArrayList<Double>();
		for(int i =0; i < n.getWeight().size();i++){
			newWeights.add( n.getWeight().get(i) + (1*(tip - n.firingFunction())*n.input.get(i)));
		}
		n.setWeights(newWeights);
		
	}
	
	
//	Layers(NeuronA ... ns){
//		for(NeuronA n : ns) {
//			n.NeuronA();
//			NeuronArray.add(n);
//		}
//	}
	
}
