package simple_nueral_net;

import java.util.ArrayList;

class NeuronA {
	
	static ArrayList<Double> input = new ArrayList<>();
	static ArrayList<Double> weights = new ArrayList<>();

	NeuronA(ArrayList<Double> inputArray){
		input.add(1.0);
		for (int i=0; i<inputArray.size();i++)  {
			input.add(inputArray.get(i));
		}
		for (int i=0; i<=inputArray.size();i++)  {
			double randomNumber = Math.random();
			System.out.println(i+" : "+randomNumber + " : " + inputArray.size());
		  weights.add(randomNumber);
		}
	}
	public ArrayList<Double> getWeight(){
		return weights;
	}
	
	public void setWeights( ArrayList<Double> newWeights){
		weights= newWeights;
	}
	
	private double activationFunction(ArrayList<Double> input, ArrayList<Double> weights){
		double dot_product=0.0;
	for (int i=0; i<input.size();i++){
		dot_product += input.get(i)*weights.get(i);
	}
	return dot_product;
	}
	public double firingFunction(){
		double firingValue = activationFunction(input,weights)>=0 ? 1.0: 0.0 ;
		return firingValue;
	}
}